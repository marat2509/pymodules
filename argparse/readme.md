| Command line | Input | Output
| ----- | ----- | ----- |
| `python3 argparse_example.py` | None | error: the following arguments are required: message |
| `python3 argparse_example.py "Hello GitLab!"` | None | Hello GitLab! |

[Docs](https://pyneng.readthedocs.io/en/latest/book/additional_info/argparse.html)
