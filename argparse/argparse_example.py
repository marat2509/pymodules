import argparse
parser = argparse.ArgumentParser(description="Test script example for argparse module")
parser.add_argument("message", help="Text message for output", type=str)
arg = parser.parse_args()
print(arg.message)
'''Running: 'python3 argparse_example.py'
Output: 'usage: argparse_example.py [-h] message
argparse_example.py: error: the following arguments are required: message'

Running: 'python3 argparse_example.py "Hello GitLab!"'
Output: 'Hello GitLab!'
'''
