import os
os.mkdir("New dir") # Creates new dir "New dir"
print(os.path.exists("New dir")) # New dir exists?
print(os.listdir('.')) # List all files in current dir
print(os.path.isdir("New dir")) # Dir "New dir" is dir?
print(os.path.isdir("os_example.py")) # File os_example.py is dir?
os.system("echo Goodbye!")
